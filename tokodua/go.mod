module tokodua

go 1.16

require (
	github.com/fsnotify/fsnotify v1.5.3 // indirect
	github.com/gorilla/mux v1.8.0 // indirect
	github.com/jinzhu/now v1.1.5 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/spf13/viper v1.11.0 // indirect
	go.mongodb.org/mongo-driver v1.9.0 // indirect
	golang.org/x/sys v0.0.0-20220422013727-9388b58f7150 // indirect
	gorm.io/driver/mysql v1.3.3 // indirect
	gorm.io/gorm v1.23.4 // indirect
)
