package models

import "gopkg.in/mgo.v2/bson"

type User struct {
	Id     bson.ObjectId `json:"id" bson: "_id"`
	name   string        `json: "name" bson: "name"`
	gender string        `json: "genfer" bson: "genfer"`
	age    int           `json: "age" bson: "age"`
}
